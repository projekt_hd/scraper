Pytania:

- Czy musimy parsowac wszystkie komentarze?
    Tak. Trzeba zastanowić się, co zrobić ze spojlerami.

- Jaki interfejs do wyświetlania bazy ma mieć aplikacja? Czy moze to byc w przegladarce?
    Pobieranie danych z bazy do aplikacji. Wyświetlanie danych w konsoli np. wybierając wszystkie filmy z danego gatunku.

- Czy user ma podawać ile stron filmów chce pobrać (1 strona = 10 filmów)
    Tak, z wybranego zakrasu [1-1000]. Jeżeli nie poda to musi pojawić się wartość domyślna.

- Czy wszystkie zagniezdzone komentarze musza sie pojawic (czy do jakiegos poziomu?)
    Nie. Nie trzeba pobierać zagnieżdzonych komentarzy.

TODO:

-- dodac wybor parametru np. gatunku, przy procesie ekstrakcji
-- napisac inserty do bazy
-- napisac selecty do wyswietlania danych w aplikacji

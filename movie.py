from itertools import count

class Movie(object):

    _ids = count(0) 

    def __init__(self, id, title, year, director, script, genre, duration, rate, votes, url):
    
        self._id = next(self._ids)
    
        #movie descritpion tags
        self.filmwebId = id
        self.title = title          # string
        self.year = year            # int
        self.director = director    # string
        self.script = script        # string
        self.genre = genre          # string ?
        self.duration = duration
        self.rate = rate            # float
        self.votes = votes          # int

        #application tags
        self.url = url
        self.transformed = False
        self.loaded = False
        self.opinions = []
        self.emptyVariablesList = []

    def addOpinion(self, opinion):
        self.opinions.append(opinion)
    
    def updateUrl(self, newUrl):
        self.url = newUrl
    
    def setTransformed(self, value):
        self.transformed = value

    def isTransformed(self):
        if self.transformed == True:
            return True
        else:
            return False

    def setLoaded(self, value):
        self.loaded = value

    def isLoaded(self):
        if self.loaded == True:
            return True
        else:
            return False


    def transformData(self):
        if(self.year != None):
            self.year = int(self.year)

        if(self.rate != None):
            self.rate = self.rate.replace(',','.')
            self.rate = float(self.rate)

        if(self.votes != None):
            self.votes = self.votes[:-6].replace(" ", "")
            self.votes = int(self.votes)

        if(self.genre != None):
            joinedGenre = ""
            for g in self.genre:
                joinedGenre += g + ","
            self.genre = joinedGenre[:-1]

        if(self.duration != None):
            self.duration = int(self.duration)

        if( (type(self.year) is int) and (type(self.rate) is float) and (type(self.votes) is int) and (type(self.duration) is int) ):
            self.setTransformed(True)


class Opinion(object):

    def __init__(self, title, text, author, date, rate, likes):

        self.title = title      # string
        self.text = text        # string
        self.author = author    # string
        self.date = date        # int ?
        self.rate = rate        # int       # 1 - 10
        self.likes = likes      # int

        #application tags
        self.url = ""
        self.transformed = False
        self.loaded = False
        self.emptyVariablesList = []

    def setTransformed(self, value):
        self.transformed = value

    def isTransformed(self):
        if self.transformed == True:
            return True
        else:
            return False

    def setLoaded(self, value):
        self.loaded = value

    def isLoaded(self):
        if self.loaded == True:
            return True
        else:
            return False

    def transformData(self):
        if(self.rate != None):
            self.rate = int(self.rate)
        else:
            self.rate = int(0)

        if(self.likes != None):
            self.likes = int(self.likes)
        else:
            self.likes = int(0)

        if(self.title != None):
            self.title = self.title.strip()
        else:
            self.title = str("")
        
        if(self.text != None):
            self.text = self.text.strip()
        else:
            self.text = str("")

        #if(self.date != "NULL"):
        # na jaki typ castowac date?

        # do poprawy... co zrobic z nullami? 
        if( ( type(self.rate) is int ) and ( type(self.likes) is int ) ):
            self.setTransformed(True)
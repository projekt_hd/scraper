import mysql.connector

class DbHelper(object):
    
    def __init__(self):
        self.username = "fwuser"
        self.password = "filmweb123"
        self.databaseName = "filmweb"
        self.host = "localhost"

    def executeInsertQuery(self, query, values):
        db = mysql.connector.connect(host=self.host, user=self.username, password=self.password, database=self.databaseName)
        cursor = db.cursor()
        cursor.execute("SET FOREIGN_KEY_CHECKS=0;")
        try:
            if values == "":
                cursor.execute(query)
            else:
                cursor.execute(query, values)
            
            db.commit()

        except mysql.connector.Error as error :
            db.rollback() 
            print("Failed inserting record into film table {}".format(error))
        
        if(db.is_connected()):
            cursor.close()
            db.close()
        
    def executeSelectLikeQuery(self, table, column, param):
        db = mysql.connector.connect(host=self.host, user=self.username, password=self.password, database=self.databaseName)
        cursor = db.cursor()
        sql_select_query = "SELECT * FROM " + table + " WHERE " + column + " LIKE %s"

        try:
            if table == "film" and param == "":
                cursor.execute("SELECT * FROM film")
            elif table == "film":
                cursor.execute(sql_select_query, ("%{}%".format(param),))
            elif table == "komentarz" and param == "":
                cursor.execute("SELECT * FROM komentarz")
            else:
                cursor.execute(sql_select_query, ("%{}%".format(param),))

            myresult = cursor.fetchall()
        
            return myresult

        except mysql.connector.Error as error :
            db.rollback() 
            print("Failed selecting records {}".format(error))
        
        if(db.is_connected()):
            cursor.close()
            db.close()
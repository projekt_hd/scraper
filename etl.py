# potrzebne do wyslania requesta GET do strony i otrzymania response
import requests
# HTML parser do wyszukiwania tagow w responsie
from bs4 import BeautifulSoup as BS
from movie import Movie, Opinion
import mysql.connector
from databaseHelper import DbHelper
import csv
import os
from prettytable import PrettyTable

class ETL(object):

    extracted_movies = []
    dbHelper = DbHelper()

    def getScriptAuthor(self, soup):
            script_span = soup.find("th", string="scenariusz:")
            for a in script_span.next_element.next_element.next_element.next_element:
                return a["title"]

    def getScriptDirector(self,soup):
        script_span = soup.find("li", {"itemprop": "director"})
        return script_span.next_element["title"]

    def getGenre(self,soup):
        genre_span = soup.find("ul", {"class": "inline sep-comma genresList"})
        for a in genre_span.next_element.next_element:
            return a

    # pobieranie oceny na stronie filmu nie jest mozliwe z uwagi na generowanie elementu przez skrypt JS
    def getRate(self,soup):
        rate_span = soup.find_all("div", {"id": "filmVoteRatingPanel"})
        for span in rate_span:
            print(span)
        return rate_span.next_element.get_text

    def getSoupFromPage(self,page_link):
        search_page = requests.get(page_link, verify=True)  # zmienna przechowujaca response ze strony wyszukiwania
        soup = BS(search_page.text, "html.parser")  # zmienna przechowujaca sprarsowany request
        return soup

    # -----------------------------------------------------------------------------------------------------------------------
    #   Fukncja wyciągające dane z opinii danego filmu
    # -----------------------------------------------------------------------------------------------------------------------

    def extractOpinions(self, movie):
        forum_url = movie.url + "/discussion"
        soup = self.getSoupFromPage(forum_url)
        counter = 0
        discussionPage_count = 2

        # <li><a class="fbtn fbtn-page" href="/Zielona.Mila/discussion?page=58">58</a></li>
        # <li><a class="fbtn fbtn-page" href="/Zielona.Mila/discussion?page=2">następna <span class="text-big">»</span></a></li>
        discussionPage_hasNextPage = soup.find("span", {"class": "text-big"})
        if (discussionPage_hasNextPage):
            discussionPage_lastPage = discussionPage_hasNextPage.previous_element.previous_element.previous_element.previous_sibling.text
            discussionPage_count = int(discussionPage_lastPage) + 1

        for i in range(1, discussionPage_count):

            discussionPage_url = forum_url + "?page=" + str(i)
            discussionSoup = self.getSoupFromPage(discussionPage_url)

            for comment in discussionSoup.find_all("li", {"class": "filmCategory"}):

                comment_title = None
                comment_title_found = comment.find("h3", {"class": "s-16"})
                if (comment_title_found):
                    # print(comment_title_found.next_element.text)
                    comment_title = comment_title_found.next_element.text

                    # co zrobic ze spoilerami?
                comment_text = None
                comment_text_found = comment.find("p", {"class": "text normal"})
                if (comment_text_found):
                    # print(comment_text_found.text)
                    comment_text = comment_text_found.text

                comment_author = None
                comment_author_found = comment.find("a", {"class": "link userNameLink"})
                if (comment_author_found):
                    # print(comment_author_found.text)
                    comment_author = comment_author_found.text

                comment_date = None
                comment_date_found = comment.find("span", {"class": "cap"})
                if (comment_date_found):
                    # print(comment_date_found["title"])
                    comment_date = comment_date_found["title"]

                comment_rated = None
                comment_rated_found = comment.find("i", {"class": "ic-star_solid primary"})
                if (comment_rated_found):
                    rate = comment_rated_found.previous_element
                    # print(rate.strip()[-1:]) #usun spacje z konca/poczatku i wez tylko ostatni znak z lancucha
                    comment_rated = rate.strip()[-1:]

                comment_likes = None
                comment_likes_found = comment.find("span", {"class": "plusCount"})
                if (comment_likes_found and (comment_likes_found.text != "")):
                    # print(comment_likes_found.text)
                    comment_likes = comment_likes_found.text

                new_opinion = Opinion(comment_title, comment_text, comment_author, comment_date, comment_rated, comment_likes)
                movie.addOpinion(new_opinion)

                counter += 1

        print("Dodano " + str(counter) + " opinii")


    # -----------------------------------------------------------------------------------------------------------------------
    #   Wyciąganie danych (szczegóły filmu + opinie) ze strony filmwebu
    # -----------------------------------------------------------------------------------------------------------------------

    def extractData(self, search_param, how_many_pages):
        # zmienna i jest odpowiednikiem strony wyszukiwania na filmwebie
        # ustawiajac range() na 100 np. range(100) zostanie pobranych 1000 najpopularniejszych tytulow filmowych
        for i in range(1, how_many_pages + 1):
            # link do strony page'a wyszukiwania na filmwebie - jeden page zawiera domyslnie 10 tytulow
            link = "http://www.filmweb.pl/search/film?q=&page=" + str(i) + search_param
            test_link = "http://www.filmweb.pl/search/film?q=&page=200" + search_param
            soup = self.getSoupFromPage(test_link)

            # html z response'a zawiera kod analogiczny do tego: <a class="filmTitle gwt-filmPage" href="/Zielona.Mila">Zielona mila</a>
            # wyciagajac wiec wartosc z tego taga mozna otrzymac tytul filmu
            # tablica zawierajaca wszystkie dopasowania dla wyszukiwanego tagu i klasy
            # movieLinks = soup.findAll("a", {"class": "filmPreview__link"})

            movies = soup.findAll("div", {"data-type": "FILM"})

            # pobieranie glownych danych
            for movie in movies:

                found_id = movie.find("div", {"class": "filmPreview__poster filmPoster filmPoster--normal "})
                if (found_id):
                    filmweb_id = found_id["data-id"]
                    print(filmweb_id)

                movie_href = movie.find("a", {"class": "filmPreview__link"})
                link = movie_href["href"]
                print(link)

                title = None
                found_title = movie.find("h3", {"class": "filmPreview__title"})
                if (found_title):
                    print("Tytuł:\t\t" + found_title.text)
                    title = found_title.text

                year = None
                found_year = movie.find("span", {"class": "filmPreview__year"})
                if (found_year):
                    year = found_year.text

                gen = []
                for genres in movie.findAll("div", {"class": "filmPreview__info filmPreview__info--genres"}):
                    for litag in genres.findAll('li'):
                        print("Gatunek:\t" + litag.text)
                        gen.append(litag.text)

                duration = None
                found_duration = movie.find("div", {"class": "filmPreview__filmTime"})
                if (found_duration):
                    print("Czas trwania:\t" + found_duration["data-duration"])
                    duration = found_duration["data-duration"]

                dir = []
                for directors in movie.findAll("div", {"class": "filmPreview__info filmPreview__info--directors"}):
                    for litag in directors.findAll('li'):
                        print("Reżyser:\t" + litag.text)
                        dir.append(litag.text)

                rate = None
                found_rate = movie.find("span", {"class": "rateBox__rate"})
                if (found_rate):
                    print("Ocena:\t\t" + found_rate.text)
                    rate = found_rate.text

                votes = None
                found_votes = movie.find("span", {"class": "rateBox__votes"})
                if (found_votes):
                    print("Liczba głosów:\t" + found_votes.text)
                    votes = found_votes.text

                url = "https://www.filmweb.pl" + link
                soup = self.getSoupFromPage(url)

                script = self.getScriptAuthor(soup)
                print("Scenariusz:\t" + script)

                # create new movie object that holds extracted data
                new_movie = Movie(filmweb_id, title, year, dir, script, gen, duration, rate, votes, url)

                # extract opinions for provided movie
                self.extractOpinions(new_movie)
                # Add movie to global list
                self.extracted_movies.append(new_movie)


    # -----------------------------------------------------------------------------------------------------------------------
    #   Transformacja danych
    # -----------------------------------------------------------------------------------------------------------------------

    def transformData(self):
        movie_list_size = len(self.extracted_movies)
        m_transformation_counter = 0
        transformedMovies = {}

        for movie in self.extracted_movies:

            transformedMovies[movie.title] = 0
            opinions_list_size = len(movie.opinions)
            o_transformation_counter = 0

            movie.transformData()
            if (movie.isTransformed):
                m_transformation_counter += 1

            for opinion in movie.opinions:
                opinion.transformData()
                if (opinion.isTransformed):
                    o_transformation_counter += 1

            transformedMovies[movie.title] = o_transformation_counter

            if (o_transformation_counter == opinions_list_size):
                print("Transformacja opinii udana - wszystkie wpisy [" + str(opinions_list_size) + "] zostały poprawnie przekonwertowane")
            else:
                print("Tranformacja opinii nieudana! Ilość wpisów na liście: " + str(opinions_list_size) + " | Przekonwertowane wpisy: " + str(len(transformedMovies)))

        if (len(transformedMovies) == movie_list_size):
            print("Transformacja filmów udana - wszystkie wpisy [" + str(movie_list_size) + "] zostały poprawnie przekonwertowane")
        else:
            print("Tranformacja nieudana! Ilość wpisów na liście: " + str(movie_list_size) + " | Przekonwertowane wpisy: " + str(len(transformedMovies)))

        print(transformedMovies)


    # -----------------------------------------------------------------------------------------------------------------------
    #   Ładowanie danych do bazy
    # -----------------------------------------------------------------------------------------------------------------------

    def loadData(self):
        for movie in self.extracted_movies:
            sql_insert_movie_query = """ REPLACE INTO `film` (`id_filmu`, `tytul_filmu`, `czas_trwania`, `rok_produkcji`, `rezyseria`, `scenariusz`, `gatunek`, `ogolna_ocena`, `liczba_glosow`) 
                                                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
            values = (movie.filmwebId, movie.title, movie.duration, movie.year, movie.director[0], movie.script, movie.genre, movie.rate, movie.votes)
            self.dbHelper.executeInsertQuery(sql_insert_movie_query,values)
            

            for comment in movie.opinions:
                sql_insert_comment_query =""" REPLACE INTO `komentarz` (`id_filmu`, `tytul_komentarza`, `tresc_watku`, `autor`, `data_utworzenia`, `ocena_autora`, `ilosc_lapek`) 
                                                    VALUES (%s,%s,%s,%s,%s,%s,%s)"""
                values = (movie.filmwebId, comment.title, comment.text, comment.author, comment.date, comment.rate, comment.likes)
                self.dbHelper.executeInsertQuery(sql_insert_comment_query,values)
                
            print ("New {"+str(len(movie.opinions)) +"} records inserted successfully into komentarz table")
        print ("New {" + str(len(self.extracted_movies)) + "} records inserted successfully into film table")
        
        self.extracted_movies = []

    def clearDatabase(self):
        sql_clear_film_table_query = """ DELETE FROM film"""
        sql_clear_komentarz_table_query = """ DELETE FROM komentarz"""
        self.dbHelper.executeInsertQuery(sql_clear_komentarz_table_query, "")
        self.dbHelper.executeInsertQuery(sql_clear_film_table_query, "")
        print ("Database cleared!")

    def printFetchedData(self, table, fetchResults):
        if table == "film":
            resultTable = PrettyTable(["id_filmu", "tytul_filmu", "czas_trwania", "rok_produkcji", "rezyseria", "scenariusz", "gatunek", "ogolna_ocena", "licza_glosow"])
        else:
            resultTable = PrettyTable(["id_filmu", "id_komentarza", "tytul_komentarza", "tresc_watku", "autor", "data_utworzenia", "ocena_autora", "ilosc_lapek"])
        
        for row in fetchResults:             
            resultTable.add_row(row)
        print(resultTable)

    def fetchData(self, table, column, param):
        result = self.dbHelper.executeSelectLikeQuery(table,column,param)
        self.printFetchedData(table,result)

    def exportDataToCsv(self, param):
        if not os.path.exists("tmp"):
            os.mkdir("tmp")
        
        if param == "":
            result = self.dbHelper.executeSelectLikeQuery("film","","")

            fp = open('tmp/film.csv', 'w')
            myFile = csv.writer(fp, delimiter=';')
            myFile.writerows(result)
            fp.close()
            print("Dane z tabeli film zapisano w lokalizacji /tmp/film.csv")

            result = self.dbHelper.executeSelectLikeQuery("komentarz","","")

            fp = open('tmp/komentarz.csv', 'w')
            myFile = csv.writer(fp, delimiter=';')
            myFile.writerows(result)
            fp.close()
            print("Dane z tabeli film zapisano w lokalizacji /tmp/komentarz.csv")
            
        else:
            result = self.dbHelper.executeSelectLikeQuery("film","id_filmu",param)
            fp = open('tmp/film_' + param + '.csv', 'w')
            myFile = csv.writer(fp, delimiter=';')
            myFile.writerows(result)
            fp.close()

            result = self.dbHelper.executeSelectLikeQuery("komentarz","id_filmu",param)
            fp = open('tmp/komentarz_' + param + '.csv', 'w')
            myFile = csv.writer(fp, delimiter=';')
            myFile.writerows(result)
            fp.close()
            print("Dane dla filmu o id " +param+ " zapisano w lokalizacji /tmp/komentarz_" +param+ ".csv oraz /tmp/film_"+param+".csv")

class Presentation:

    def write_database(self,action):
        data_extracted = False
        data_transformed = False
        while True:
            try:
                number = int(input('Wybierz: \n1. E - pobranie danych ze strony www\n2. T - transformacja danych\n3. L - zapis danych do bazy\n4. ETL - algorytm ETL\n'))
            except ValueError:
                print('Wartosc musi byc integerem!')
                continue
            if number not in [1,2,3,4]:
                print('Zly numer!')
                continue
            elif number == 1:
                searchParam = self.additionalSearchParam()
                how_many_pages = Presentation.howmanypages(self)
                action.extractData(searchParam, how_many_pages)
                data_extracted = True
                print("Przetworzono: " + str(len(action.extracted_movies)) + " filmow")
            elif number == 2:
                if data_extracted == True and data_transformed == False:
                    action.transformData()
                    data_transformed = True
                else:
                    print("Zanim przetransformujesz dane, najpierw musisz je pobrac!")
                    continue
            elif number == 3:
                if data_transformed == True:
                    action.loadData()
                    break
                else:
                    print("Zanim zaladujesz dane do bazy, najpierw musisz je przetworzyc!")
                    continue
            else:
                searchParam = self.additionalSearchParam()
                how_many_pages = Presentation.howmanypages(self)
                action.extractData(searchParam,how_many_pages)
                action.transformData()
                action.loadData()
                print("Przetworzono: " + str(len(action.extracted_movies)) + " filmow")
                self.get_user_input(action)
                break

    def print_database(self,action):
        while True:
            table = input("wybierz tabele [film / komentarz / wszystkie]\n")
            if table not in ["film","komentarz","wszystkie"]:
                print('Brak takiej opcji!')
                continue
            
            if table == "wszystkie":
                action.fetchData("film","","")
                action.fetchData("komentarz","","")
            elif table == "film":
                print(" id_filmu | tytul_filmu | czas_trwania | rok_produkcji | rezyseria | scenariusz | gatunek | ogolna_ocena | licza_glosow \n")
                column = input('Podaj filtrowaną kolumnę lub wciśnij enter aby kontynuować\n')
                if column not in ["id_filmu","tytul_filmu","czas_trwania","rok_produkcji","rezyseria","scenariusz","gatunek","ogolna_ocena","licza_glosow",""]:
                    print('Brak takiej kolumny!')
                    continue
                if column == "":
                    action.fetchData("film","","")  
                else:
                    database_filter = input('Czego szukasz?:\n')
                    action.fetchData("film", column, database_filter)
                break
            else:
                print(" id_komentarza | id_filmu | tytul_komentarza | tresc_watku | autor | data_utworzenia | ocena_autora | ilosc_lapek \n")
                column = input('Podaj filtrowaną kolumnę lub wciśnij enter aby kontynuować:\n')
                if column not in ["id_komentarza","id_filmu","tytul_komentarza","tresc_watku","autor","data_utworzenia","ocena_autora","ilosc_lapek",""]:
                    print('Brak takiej kolumny!')
                    continue
                if column == "":
                    action.fetchData("komentarz","","")
                else:
                    database_filter = input('Czego szukasz?:\n')
                    action.fetchData("komentarz", column, database_filter)
                break
        self.get_user_input(action)    

    def clean_database(self,action):
        action.clearDatabase()

    def export_database(self,action):
        userChoice = input("Wciśnij enter aby wyeksportować wszystkie dane z bazy lub podaj id filmu którego informacje chesz wyeksportować (wraz z komentarzami)")
        if userChoice == "":
            action.exportDataToCsv("")
        else:
            action.exportDataToCsv(userChoice)

    def howmanypages(self):
         while True:
            try:
                how_many_pages = int(input('Ile stron pobrac? \n'))
                return how_many_pages
            except ValueError:
                print('Wartosc musi byc integerem!')
                continue

    def additionalSearchParam(self):
        searchParam = ""
        userChoice = input("Podaj dodatkowe parametry (zakres lat [komenda lata] lub gatunek [komenda gatunek]). Wciśnij enter aby pominąć dodatkowe parametry\n")
        if userChoice not in ["lata", "gatunek", ""]:
            print("podano błędny parametr!\n")
        elif userChoice == "lata":
            yearStart = input("Rok produkcji od: ")
            yearEnd = input("Rok produkcji do: ")
            searchParam = "&startYear=" + yearStart + "&endYear=" + yearEnd
        elif userChoice == "gatunek":
            print("Dostępne gatunki:\n akcja / dokumentalny / familijny / horror / dramat / fantasy / komedia / krótkometrażowy\n                kryminał / melodramat / niemy / przygodowy / romans / scfi-fi / thriller / animacja\n")
            name = input("Podaj nazwe wybranego gatunku: ")
            searchParam = "&genres=" + str(self.mapGenreToNumber(name))
        else:
            pass
        return searchParam

    def mapGenreToNumber(self, genre):
        genres = {
            "akcja" : 28,
            "dokumentalny" : 5,
            "familijny" : 8,
            "horror" : 12,
            "dramat" : 6,
            "fantasy" : 9,
            "komedia" : 13,
            "krótkometrażowy" : 50,
            "kryminał" : 15,
            "melodramat" : 16,
            "niemy" : 67,
            "przygodowy" : 20,
            "romans" : 32,
            "sci-fi" : 33,
            "thriller" : 24,
            "animacja" : 2
        }
        return genres.get(genre)

    def get_user_input(self,action):
        while True:
            try:
                user_input = int(input('Wybierz:\n1. Operacje na bazie\n2. Wyswietlanie bazy\n3.Czyszczenie bazy\n4.Eksport danych do pliku CSV\n'))
            except ValueError:
                print('Wartosc musi byc integerem!')
                continue
            if user_input not in [1,2,3,4]:
                print('Zly numer!')
                continue
            elif user_input == 1:
                self.write_database(action)
            elif user_input == 2:
                self.print_database(action)
            elif user_input == 3:
                self.clean_database(action)
            else:
                self.export_database(action)
            break

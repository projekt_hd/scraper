from etl import ETL
from presentation import Presentation

if __name__ == "__main__":
    etl = ETL()
    presentation = Presentation()
    presentation.get_user_input(etl)